import { call, put, takeLatest, race, take } from 'redux-saga/effects';
import { types as ${storeName}Types, actions as ${storeName}Actions } from '../ducks/${storeName}';
import axios from 'axios';

function* (action) {
	try {
		
		yield put(${storeName}Actions.${storeName}FetchSuccess( // something ));
	} catch (e) {
		action = ${storeName}Actions.${storeName}FetchFailed(e.message)
		yield put(action)
	}
}


function* ${NAME}Saga() {
	yield takeLatest(//action , // saga name)
}

export {
	${NAME}Saga
}