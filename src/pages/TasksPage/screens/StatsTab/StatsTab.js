import React from 'react';
import { SubTitle, Container, Card, Title, Button} from 'vendor/bulma';
import { connect } from 'react-redux';
import { actions as statsActions } from 'ducks/stats';
import Spinner from 'components/Spinner';
import { Bar as BarChart, Pie as PieChart } from 'react-chartjs-2';
import { StatsLevel as UserStatsLevel, Leaderboards, TopUsersContainer } from 'features/stats';
import Streak from 'components/Streak';

const ChurnStatsText = (props) => (
    <div>
        <SubTitle is={4}>
            <strong>Today you've done {props.userStats.done_today} tasks,</strong>
            <br /> and have {props.userStats.remaining_tasks} remaining.
        </SubTitle>
        <SubTitle is={4}>
            <strong>Your task churn median is {props.userStats.tda} tasks/day,</strong>
            <br /> compared to the world average {props.worldStats.tda} tasks/day.
        </SubTitle>
        <SubTitle is={4}>
            {props.userStats.streak > 0 &&
            <div>
                <strong>You have a <Streak days={props.userStats.streak}/> day streak going.</strong> Keep it up!
            </div>
            }

            {props.userStats.streak === 0 &&
            <div>
                <strong>You don't have a streak going.</strong> Add tasks every day to start one!
            </div>
            }
        </SubTitle>
    </div>

)

class StatsTab extends React.Component {
    render() {
        if (!this.props.ready && !this.props.failed) {
            return <Container><center><Spinner text={"Loading your stats..."} /></center></Container>;
        } else if (this.props.failed) {
            return <div>Failed to load stats. <Button onClick={this.props.loadStats}>Reload</Button></div>
        }

        return (
            <div className="StatsTab">
                <Container style={{paddingTop: 30}}>
                    <Title>
                        You
                    </Title>

                    <div className={"columns"}>
                        <div className={"column"}>
                            <Card>
                                <Card.Content>
                                    <ChurnStatsText worldStats={this.props.worldStats} userStats={this.props.userStats} />
                                </Card.Content>
                            </Card>
                        </div>

                        <div className={"column"}>
                            <Card>
                                <Card.Content>
                                    <UserStatsLevel />
                                </Card.Content>
                            </Card>
                        </div>
                    </div>

                    <br/>

                    <Title>
                        Charts
                    </Title>
                    <Card>
                        <Card.Content>
                            <div className="columns">
                                <div className="column">
                                    <div className="column">
                                        <SubTitle>Tasks per project</SubTitle>
                                        {this.props.userStats.tasks_per_project &&
                                        <PieChart data={this.props.userStats.tasks_per_project} />}
                                    </div>

                                </div>
                                <div className="column">
                                    <SubTitle>This week reviewed</SubTitle>
                                    {this.props.userStats.done_week &&
                                    <BarChart data={this.props.userStats.done_week} />}
                                </div>
                            </div>
                        </Card.Content>
                    </Card>

                    <br />

                    <Title>
                        Leaderboards
                    </Title>
                    <Card style={{ overflowX: 'auto' }}>
                        <Card.Content>
                            <TopUsersContainer component={Leaderboards} />
                        </Card.Content>
                    </Card>
                    <br />
                </Container>
            </div>
        )
    }
}



const mapStateToProps = (state) => {
    return {
        ready: state.stats.ready,
        isLoading: state.stats.isLoading,
        failed: state.stats.failed,
        userStats: state.stats.user,
        worldStats: state.stats.world,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loadStats: () => dispatch(statsActions.fetchStats())
    }
}

StatsTab.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StatsTab);