import React from 'react';
import PropTypes from 'prop-types';
import {Level, SubTitle} from "vendor/bulma";
import Streak from "components/Streak";

const StatsLevelItem = (props) => (
    <Level.Item hasTextCentered>
        <div>
            <p className="heading">{props.subtitle}</p>
            <p className="title">
                {props.children}
            </p>
        </div>
    </Level.Item>
)

const StatsLevel = ({ userStats, worldStats, small=false }) => (
    <Level className={"is-primary"}>
        <Level.Left>
            <Level.Item>
                <SubTitle is='4'>
                    Here's your stats.
                </SubTitle>
            </Level.Item>
        </Level.Left>
        <Level.Right>
            <StatsLevelItem subtitle={"Streak"} small={small}>
                <Streak days={userStats.streak} endingSoon={userStats.streak_ending_soon} />
            </StatsLevelItem>
            <StatsLevelItem subtitle={"tasks/day avg."} small={small}>
                {userStats.tda}
            </StatsLevelItem>
            <StatsLevelItem subtitle={"World tasks/day avg."} small={small}>
                {worldStats.tda}
            </StatsLevelItem>
        </Level.Right>
    </Level>
);

StatsLevel.propTypes = {
    userStats: PropTypes.shape({
        streak: PropTypes.number,
        streak_ending_soon: PropTypes.boolean,
        tda: PropTypes.number,
    }),
    worldStats: PropTypes.shape({
        tda: PropTypes.number,
    })
}

StatsLevelItem.propTypes = {
    subtitle: PropTypes.string.isRequired
}


export {
    StatsLevel,
    StatsLevelItem
};