import React from 'react';
import { connect } from 'react-redux';
import ServiceCard from './components/ServiceCard';
import {SubTitle} from "vendor/bulma";
import {mapStateToProps, mapDispatchToProps} from "ducks/apps";

const AppsList = (props) => {
    return (
        <div>
            <SubTitle className="has-text-grey" is="4">
                Chat
            </SubTitle>
            <ServiceCard
                name="Slack"
                logo="https://cdn.worldvectorlogo.com/logos/slack-1.svg"
                summary="Seamlessly add logs from Slack, see your friend's stats, and stay updated right from your chat client."
                to="/tasks/apps/slack"/>
            <SubTitle className="has-text-grey" is="4">
                To-dos
            </SubTitle>
            <ServiceCard
                name="Todoist"
                logo="https://blog.todoist.com/wp-content/uploads/2015/09/todoist-logo.png"
                summary="Link Todoist projects to Makerlog, and log straight from your phone or web."
                to="/tasks/apps/todoist"
                installed={props.apps['todoist'] ? props.apps['todoist'].installed : false}/>
            <SubTitle className="has-text-grey" is="4">
                Boards
            </SubTitle>
            <ServiceCard
                name="Trello"
                logo="http://www.stickpng.com/assets/images/58482beecef1014c0b5e4a36.png"
                summary="Mark a card as done and we'll log it. It's awesome."
                to="/tasks/apps/trello"
                installed={props.apps['trello'] ? props.apps['trello'].installed : false}/>
            <SubTitle className="has-text-grey" is="4">
                Developers
            </SubTitle>
            <ServiceCard
                name="GitHub"
                logo="https://png.icons8.com/metro/1600/github.png"
                summary="Log your commits to projects automagically"
                installed={props.apps['github'] ? props.apps['github'].installed : false}
                to="/tasks/apps/github"/>
            <ServiceCard
                name="GitLab"
                logo="https://png.icons8.com/color/1600/gitlab.png"
                summary="Log your commits to projects automagically"
                installed={props.apps['gitlab'] ? props.apps['gitlab'].installed : false}
                to="/tasks/apps/gitlab"/>
            <ServiceCard
                name="NodeHost"
                logo="https://www.nodehost.ca/logo.png"
                summary="NodeHost is the next generation of web hosting. Log your actions and see what others are building."
                installed={props.apps['nodehost'] ? props.apps['nodehost'].installed : false}
                to="/tasks/apps/nodehost"/>
            <ServiceCard
                name="Your own apps"
                logo="https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Gear_icon-72a7cf.svg/2000px-Gear_icon-72a7cf.svg.png"
                installed={props.apps['webhook'] ? props.apps['webhook'].installed : false}
                summary="Use webhooks to log from your apps, and make Makerlog truly yours."
                to="/tasks/apps/webhook"/>
        </div>
    )
}

AppsList.propTypes = {}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(AppsList);