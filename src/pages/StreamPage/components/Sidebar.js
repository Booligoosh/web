import React from 'react';
import AdCard from "components/sidebar/AdCard";
import FooterCard from "components/sidebar/FooterCard";
import AppCard from "components/sidebar/AppCard";
import RecentDiscussionsCard from "components/sidebar/RecentDiscussionsCard";
import {PeopleCard} from "features/users";
import {MyProductsCard} from 'features/products';

export default (props) => (
    <>
        <PeopleCard recentlyLaunched />
        <br />

        <RecentDiscussionsCard />
        <br />

        <MyProductsCard />
        <hr />
        <AdCard />
        <br />
        <AppCard />
        <br />
        <FooterCard />

    </>
)