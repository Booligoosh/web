import React from 'react';
import {StatsLevel} from 'features/stats';
import { connect } from 'react-redux';
import { actions as editorActions } from 'ducks/editor';
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import styled from 'styled-components';
import { Hero, Container, Title, Button, Icon } from 'vendor/bulma';
import Greeting from "components/Greeting";
import { FullName } from "features/users";
import { darker } from 'styles/colors';
import HeaderTrend from "./HeaderTrend";
import { withTheme } from 'styled-components';
import DayProgressBar from "./DayProgressBar";
import chroma from 'chroma-js';

const StyledHero = styled(Hero)`
  background-color: ${props => props.theme.primaryDarker};
  margin-bottom: 25px;
  position: relative;
  
    .container {
      z-index: 10;
    }
    
    .column {
      z-index: 10;
    }
    
    .hero-body {
    }
  
    .level p.title {
      color: white !important;
    }
    
    .stats-column {
      display: flex;
      justify-content: flex-end;
      align-items: center;
    }
    
    .trend {
      z-index: 1;
      position: absolute;
      bottom: 0;
      right: 0;
      height: 100%;
      width: 50%;
    }
`

const TrendCover = styled.span`
  z-index: 2;
  position: absolute;
  height: 100%;
  width: 100%;
  background: linear-gradient(90deg, ${props => props.color} 15%, ${props => { let color = chroma(props.color).alpha(0).rgba(); return `rgba(${color[0]}, ${color[1]}, ${color[2]}, ${color[3]})`}} 100%);
`

const CreateButton = styled(Button)`
    font-size: 1.2rem;
    font-weight: bold;
    font-family: var(--font);
    padding-left: 1.5em !important;
    padding-right: 1.5em !important;
    background-color: ${props => darker(props.theme.primaryDarker).hex()} !important;
`

const StreamHeader = withTheme((props) => (
    <StyledHero>
        <StyledHero.Body>
            <Container>
                <div className={"columns"}>
                    <div className={"column"}>
                        <Title className={"has-text-white"} is={'5'} style={{marginBottom: 20, fontSize: '1.10rem'}}>
                            <Greeting withEmoji />, <FullName user={props.user} />
                        </Title>
                        <div>
                            <CreateButton onClick={props.toggleEditor} className={"is-rounded"} primary>
                                <Icon><FontAwesomeIcon icon='plus-square' /></Icon> Add a new task
                            </CreateButton>
                        </div>
                    </div>
                    <div className="stats-column column is-hidden-mobile">
                        <StatsLevel white importantOnly className="is-pulled-right is-mobile has-text-white" />
                    </div>
                </div>
            </Container>
            <DayProgressBar />
            {props.trend &&
                <div className={"trend"}>
                    <TrendCover color={darker(props.theme.primaryDarker, 0.0).css()}></TrendCover>
                    <HeaderTrend color={darker(props.theme.primaryDarker, 0.3).hex()} trend={props.trend} />
                </div>
            }
        </StyledHero.Body>
    </StyledHero>
))

const mapDispatchToProps = (dispatch) => ({
    toggleEditor: () => dispatch(editorActions.toggleEditor())
})

const mapStateToProps = (state) => ({
    user: state.user.me,
    trend: state.stats.user ? state.stats.user.activity_trend : null,
})

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(StreamHeader);