import React from 'react';
import { Hero, SubTitle, Title, Card, Heading, Level } from 'vendor/bulma';
import { Container } from 'vendor/bulma';
import { Line as LineChart, Bar as BarChart } from 'react-chartjs-2';
import axios from 'axios';
import Streak from 'components/Streak';
import Page from 'layouts/Page';

class NewUsersGraph extends React.Component {
    render() {
        return <LineChart data={this.props.data} />
    }
}

class TasksDayGraph extends React.Component {
    render() {
        return <BarChart data={this.props.data} />
    }
}

class OpenPage extends React.Component {
    state = {
        loading: true,
        data: null,
        failed: false,
    }

    componentDidMount() {
        this.fetchGraphData()
    }

    fetchGraphData = async () => {
        try {
            let data = await axios.get('/open/')
            data = data.data
            let tda1 = data.done_per_day
            let tda2 = data.remaining_per_day
            data.users_per_day_graph = {
                labels: Object.keys(data.new_users_per_day),
                datasets: [
                    {
                        label: 'User count',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                        data: Object.values(data.new_users_per_day)
                    }
                ]
            }

            data.tasks_per_day_graph = {
                labels: Object.keys(tda1),
                datasets: [
                    {
                        label: 'Completed tasks per day',
                        backgroundColor: 'rgba(255,99,132,0.2)',
                        borderColor: 'rgba(255,99,132,1)',
                        borderWidth: 1,
                        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                        hoverBorderColor: 'rgba(255,99,132,1)',
                      
                        data: Object.values(tda1)
                    },
                    {
                        label: 'Remaining tasks per day',
                        borderWidth: 1,
                        data: Object.values(tda2)
                    }
                ]
            }
            this.setState({
                loading: false,
                data: data,
                failed: false,
            })
        } catch (e) {

        }
    }

    render() {
        return (
            <Page className="OpenPage" loading={this.state.loading} contained={false}>
                <Hero primary>
                    <Container>
                        <Hero.Body>
                            <div className={"columns"}>
                                <div className={"column"}>
                                    <Title is={"3"}>
                                        Makerlog is an open product.
                                    </Title>
                                    <SubTitle>
                                        We openly share statistics on growth and worldwide productivity.
                                    </SubTitle>
                                </div>
                            </div>
                        </Hero.Body>
                    </Container>
                </Hero>
                
                <br />

                <Container>

                    {this.state.data &&
                        <Card>
                            <Card.Content>
                                <Level>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>User count</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.user_count}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>All tasks</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.tasks_all}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Tasks done</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.tasks_done}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Tasks remaining</Heading>
                                            <Title is={3}>{this.state.data && this.state.data.tasks_all - this.state.data.tasks_done}</Title>
                                        </div>
                                    </Level.Item>
                                    <Level.Item hasTextCentered>
                                        <div>
                                            <Heading>Highest streak</Heading>
                                            <Title is={3}>{this.state.data && <Streak days={this.state.data.highest_streak} />}</Title>
                                        </div>
                                    </Level.Item>
                                </Level>
                            </Card.Content>
                        </Card>
                    }
                    <br />
                    <div className="columns">
                        <div className="column">
                            {this.state.data && this.state.data.users_per_day_graph &&
                                <Card>
                                    <Card.Content>
                                        <NewUsersGraph data={this.state.data.users_per_day_graph} />
                                    </Card.Content>
                                </Card>
                            }
                        </div>
                        <div className="column">
                            {this.state.data && this.state.data.tasks_per_day_graph &&
                                <Card>
                                    <Card.Content>
                                        <TasksDayGraph data={this.state.data.tasks_per_day_graph} />
                                    </Card.Content>
                                </Card>
                            }
                        </div>
                    </div>
                </Container>
            </Page>
        )
    }
}

OpenPage.propTypes = {}

export default OpenPage;