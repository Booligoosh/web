import React from 'react';
import {getTask} from "lib/tasks";
import {Redirect} from "react-router-dom";
import {Button, Title} from "vendor/bulma";
import {UserHero} from "features/users";
import {CommentsBox} from "features/comments";
import {EntryDetail} from "features/stream";
import LoggedOutMessage from '../../components/LoggedOutMessage';
import Helmet from 'react-helmet-async';
import Page from "../../layouts/Page";

class EntryPage extends React.Component {
    state = {
        loading: true,
        task: null,
        notFound: false,
        failed: false,
    }

    async fetchEntry() {
        this.setState({ loading: true, failed: false, })
        try {
            const task = await getTask(this.props.match.params.id);
            this.setState({ task: task, loading: false, failed: false, })
        } catch (e) {
            this.setState({ task: null, loading: false, failed: true, })
            if (e.status_code && e.status_code === 404) {
                this.setState({ failed: true, notFound: true })
            } else {
                this.setState({ failed: true })
            }
        }
    }

    componentDidMount() {
        this.fetchEntry();
    }

    render() {
        if (this.state.notFound) {
            return <Redirect to="/404" />
        }

        if (this.state.failed) {
            return (
                <center>
                    <Title>
                        Oops! There was a network error. <Button text onClick={this.fetchEntry}>Retry</Button>
                    </Title>
                </center>
            )
        }

        return (
            <Page contained={false} loading={this.state.loading} className="EntryPage">
                {this.state.task && this.state.task.user &&
                    <>
                        <Helmet>
                            <title>Done by @{this.state.task.user.username} | Makerlog</title>
                            <meta name="description" content={`${this.state.task.done ? "✅" : "🕐"} ${this.state.task.content}`} />
                            <meta name="twitter:card" content={this.state.task.attachment ? 'summary_large_image': 'summary'} />
                            <meta name="twitter:site" content="@getmakerlog" />
                            <meta name="twitter:title" content={`Done by @${this.state.task.user.username} on Makerlog`} />
                            <meta name="twitter:description" content={`✅ ${this.state.task.content}`} />
                            <meta name="twitter:image" content={this.state.task.attachment ? this.state.task.attachment : this.state.task.user.avatar} />
                        </Helmet>
                        <UserHero user={this.state.task.user} />
                        <br />
                        <div className={"container"}>
                            <LoggedOutMessage />
                            <EntryDetail task={this.state.task} />
                            <CommentsBox task={this.state.task} />
                        </div>
                    </>
                }
            </Page>
        )
    }
}

EntryPage.propTypes = {}

export default EntryPage;