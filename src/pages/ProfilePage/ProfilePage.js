import React from 'react';
import {Box, Title, Icon, Level, Heading, Button, Card} from "vendor/bulma";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import Spinner from "../../components/Spinner";
import Streak from "../../components/Streak";
import Tda from "../../components/Tda";
import {connect} from "react-redux";
import {getByUsername} from "../../lib/user";
import {Redirect} from "react-router-dom";
import {getUserStats} from "../../lib/stats";
import Praise from "../../components/Praise";
import Sticky from 'react-stickynode';
import Emoji from "../../components/Emoji";
import {Avatar, FollowButton as GatedFollowButton, FullName} from "features/users";
import {UserStream} from "../../features/stream";
import {ProductsContainer, ProductList} from "features/products";
import Linkify from "react-linkify";
import Helmet from 'react-helmet-async';
import Page from "layouts/Page";
import { UserActivityGraph } from "features/stats";

const SocialStatsBadges = ({ children }) => (
    <div className={`social-count`}>
        {children}
    </div>
)


const TwitterSocialStats = ({ followers }) => (
    <Icon><FontAwesomeIcon size={"lg"} icon={['fab', 'twitter']} color={'#1DA1F2'} /></Icon>
)

const GitHubSocialStats = ({ followers }) => (
    <Icon><FontAwesomeIcon size={"lg"} icon={['fab', 'github']} color={'black'} /></Icon>
)

const InstagramSocialStats = ({ followers }) => (
    <Icon><FontAwesomeIcon size={"lg"} icon={['fab', 'instagram']} color={'#8a3ab9'} /></Icon>
)

const ProductHuntSocialStats = ({ followers }) => (
    <Icon><FontAwesomeIcon size={"lg"} icon={['fab', 'product-hunt']} color={'#f07810'} /></Icon>
)



class SocialStatsContainer extends React.Component {
    render() {
        if (!this.props.user) return null;

        if (!this.props.user.twitter_handle &&
            !this.props.user.github_handle &&
            !this.props.user.instagram_handle &&
            !this.props.user.product_hunt_handle) return null;

        return (
            <SocialStatsBadges>
                {this.props.user.twitter_handle &&
                    <a href={`https://twitter.com/${this.props.user.twitter_handle}`} target='_blank' rel="noopener noreferrer">
                        <TwitterSocialStats />
                    </a>
                }
                {this.props.user.github_handle &&
                    <a href={`https://github.com/${this.props.user.github_handle}`} target='_blank' rel="noopener noreferrer">
                        <GitHubSocialStats />
                    </a>
                }
                {this.props.user.instagram_handle &&
                    <a href={`https://instagram.com/${this.props.user.instagram_handle}`} target='_blank' rel="noopener noreferrer">
                        <InstagramSocialStats />
                    </a>
                }
                {this.props.user.product_hunt_handle &&
                    <a href={`https://producthunt.com/@${this.props.user.product_hunt_handle}`} target='_blank' rel="noopener noreferrer">
                        <ProductHuntSocialStats />
                    </a>
                }
            </SocialStatsBadges>
        )
    }
}

const UserCard = ({ user }) => (
    <Box className={"user-box"}>
        <center>
            <Avatar aura={'basic'} user={user} is={128} />
        </center>
        <p className={"bio"}>
            <Linkify properties={{'target': '_blank', 'rel': 'nofollow noopener noreferrer'}}>
                {user.description ? user.description : "I have no bio... yet!"}
            </Linkify>
            <SocialStatsContainer user={user} />
        </p>
        <p className={"cta"}>
            <GatedFollowButton userId={user.id} />
        </p>
    </Box>
)

class ProfileBarStats extends React.Component {
    render() {
        const stats = this.props.stats;

        return (
            <Level.Right className={"ProfileBarStats is-hidden-mobile"}>
                <Level.Item hasTextCentered>
                    <Heading>Streak</Heading>
                    <Title is={"3"}><Streak days={stats.streak} /></Title>
                </Level.Item>
                <Level.Item></Level.Item>
                <Level.Item hasTextCentered>
                    <Heading>Tasks/day</Heading>
                    <Title is={"3"}><Tda tda={stats.tda} /></Title>
                </Level.Item>
                <Level.Item></Level.Item>
                <Level.Item hasTextCentered>
                    <Heading>Followers</Heading>
                    <Title is={"3"}><Emoji emoji={"👥"} /> {stats.follower_count}</Title>
                </Level.Item>
                <Level.Item></Level.Item>
                <Level.Item hasTextCentered>
                    <Heading>Praise</Heading>
                    <Title is={"3"}><Praise praise={stats.praise_received}/></Title>
                </Level.Item>
            </Level.Right>
        )
    }
}

const ProfileBar = ({ user, stats }) => (
    <React.Fragment>
        <div className={"blur-container"}>
            <div className={"blur"}></div>
        </div>
        <div className={"stats-container"}>
            <div className={"container"}>
                <div className={"columns is-variable is-5"}>
                    <div className={"column is-3 is-hidden-mobile user-card-container"}>
                        <UserCard user={user} />
                    </div>
                    <div className={"column stats-bar"}>
                        <Level>
                            <Level.Left>
                                <Level.Item>
                                    <Title is={"3"} className={"is-brand-green"}>
                                        <FullName user={user} />
                                    </Title>
                                </Level.Item>
                            </Level.Left>
                            <ProfileBarStats stats={stats} />
                        </Level>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>
)

class ProfilePage extends React.Component {
    state = {
        isLoading: false,
        user: null,
        stats: null,
        failed: false,
        notFound: false,
    }

    getUser = async () => {
        this.setState({ failed: false, notFound: false, isLoading: true })
        try {
            const user = await getByUsername(this.props.match.params.username);
            const stats = await getUserStats(user.id);
            this.setState({ user: user, stats: stats, isLoading: false })
        } catch (e) {
            if (e.status_code && e.status_code === 404) {
                this.setState({ failed: true, notFound: true })
            } else {
                this.setState({ failed: true })
            }
        }
    }

    async componentDidMount() {
        await this.getUser();
        this.injectCss();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.username !== prevProps.match.params.username) {
            this.getUser();
        }
    }

    componentWillUnmount() {
        this.removeCss()
    }

    injectCss = () => {
        const nav = document.getElementById('main-navbar');
        nav.classList.add("translucent-navbar");
    }

    removeCss = () => {
        const nav = document.getElementById('main-navbar');
        if (nav && nav.classList.contains("translucent-navbar")) {
            nav.classList.remove("translucent-navbar");
        }
    }


    render() {
        if (this.state.notFound) {
            return <Redirect to="/404" />
        }

        if (this.state.isLoading) {
            return <Page loading={true}></Page>
        }

        if (this.state.failed) {
            return (
                <center>
                    <Title>
                        Oops! There was a network error. <Button text onClick={this.getUser}>Retry</Button>
                    </Title>
                </center>
            )
        }

        if (!this.state.failed && !this.state.isLoading && this.state.user) {
            return (
                <Page contained={false} footer={false} className="UserPage">
                    <Helmet>
                        <title>@{this.state.user.username} | Makerlog</title>
                        <meta name="description" content={`${this.state.user.username} is on Makerlog, the world's most supportive community of makers shipping together.`} />
                        <meta name="twitter:card" content={'summary'} />
                        <meta name="twitter:site" content="@getmakerlog" />
                        <meta name="twitter:title" content={`@${this.state.user.username} on Makerlog`} />
                        <meta name="twitter:description" content={this.state.user.description ? this.state.user.description : `${this.state.user.username} is on Makerlog, the world's most supportive community of makers shipping together.`} />
                        {this.state.user.avatar &&
                        <meta name="twitter:image" content={this.state.user.avatar} />
                        }
                    </Helmet>
                    <section className="hero is-medium is-dark has-brand-gradient" style={this.state.user.header && {backgroundImage: `url(${this.state.user.header})`}}>
                        <div className="hero-body">
                        </div>
                        <ProfileBar stats={this.state.stats} user={this.state.user} />
                    </section>

                    <div className={"container"}>
                        <div className={"columns is-variable is-4"}>
                            <div className={"column is-3 card-column is-hidden-mobile"}>
                                <Sticky enabled={true} top={30}>
                                    <Card>
                                        <Card.Content>
                                            {this.state.user &&
                                                <UserActivityGraph user={this.state.user} />
                                            }
                                        </Card.Content>
                                    </Card>
                                    <br />
                                    <Card>
                                        <Card.Content>
                                            <Heading>
                                                Products
                                            </Heading>
                                            <div style={{marginTop: 10}}>
                                                <ProductsContainer user={this.state.user.id} component={
                                                    ({ products }) =>
                                                        <ProductList thumbnail products={products} />
                                                } />
                                            </div>
                                        </Card.Content>
                                    </Card>
                                </Sticky>
                            </div>
                            <div className={"column stream-column"}>
                                <div className={"is-hidden-tablet"}>
                                    <UserCard user={this.state.user} /><br />
                                </div>
                                <UserStream userId={this.state.user.id} />
                            </div>
                        </div>
                    </div>
                </Page>
            )
        } else {
            return (
                <center>
                    <Spinner />
                </center>
            );
        }
    }
}

ProfilePage.propTypes = {}

const mapStateToProps = (state) => ({
    me: state.user.me,
    isLoggedIn: state.auth.loggedIn,
})

export default connect(
    mapStateToProps
)(ProfilePage);