import React from 'react';
import {Card} from "vendor/bulma";
import { withTheme } from 'styled-components';

const FooterCard = (props) => (
    <Card className={"CardFooter"}>
        <Card.Content>
            <div className="columns">
                <div className="column has-vertically-aligned-content branded">
                    <p>
                        <a href="https://sergiomattei.com" target="_blank" rel="noopener noreferrer">
                            <img alt="Arkus" src={props.theme.isDark ? "https://sergiomattei.com/img/Arkus-Fatter.png" : "https://sergiomattei.com/img/Arkus-Fatter-Black.png"} />
                        </a>
                    </p>
                </div>
                <div className={"column copy"}>
                    <p>
                        <p>
                            <strong>Made in Puerto Rico</strong>
                        </p>
                        <p>
                            <small>© Sergio Mattei</small>
                        </p>
                    </p>
                </div>
            </div>
        </Card.Content>
    </Card>
);

export default withTheme(FooterCard);