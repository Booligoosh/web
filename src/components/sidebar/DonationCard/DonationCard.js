import React from 'react';
import DonationButton from "./DonationButton/index";
import {Card} from "vendor/bulma";

const DonationCard = (props) => (
    <div className="DonationCard">
        <Card>
            <Card.Content>
                <center>
                    <p>
                        <DonationButton />
                    </p><br />
                    <small className={"explanation"}>
                        <span className="has-text-grey">Donations help keep Makerlog free and open.</span>
                    </small>
                </center>
            </Card.Content>
        </Card>
    </div>
)

DonationCard.propTypes = {}

export default DonationCard;