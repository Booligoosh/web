import React from 'react';
import AdBlockDetect from "../../vendor/AdBlockDetect";
import DonationCard from "./DonationCard";
import CarbonAd from "../../vendor/CarbonAd";

export default () => (
    <React.Fragment>
        <AdBlockDetect>
            <DonationCard/>
        </AdBlockDetect>
        <CarbonAd />
    </React.Fragment>
)