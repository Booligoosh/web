import React from 'react';
import {Card, Level, Media} from "../../vendor/bulma";
import Emoji from "../Emoji";
import OutboundLink from "../OutboundLink";
import { UserContainer } from "features/users";
import Avatar from "../../features/users/components/Avatar/Avatar";

export default () => (
    <Card>
        <Card.Content>
            <Media>
                <Media.Content>
                    <p className="heading"><Emoji emoji="📱"/> Get the app</p>
                    <p>
                        Get the Makerlog app and log tasks from anywhere in the world.
                    </p>
                    <p>
                        Get it on <OutboundLink href="https://testflight.apple.com/join/n3zhTJtu">iOS</OutboundLink> or  <OutboundLink href="https://play.google.com/store/apps/details?id=com.brownfingers.getmakerlog">Android</OutboundLink>.
                    </p>
                    <br />
                    <Level>
                        <Level.Left>
                            <Level.Item>
                                <UserContainer username="arnavpuri" component={({ user }) => <Avatar is={16} user={user} />} />
                            </Level.Item>
                            <Level.Item>
                                <small>by maker&nbsp;<a href="https://getmakerlog.com/@arnavpuri">@arnavpuri</a></small>
                            </Level.Item>
                        </Level.Left>
                    </Level>
                </Media.Content>
                <Media.Right>
                    <img alt="app screenshot" src="https://i.imgur.com/UTwrPHl.png" width="70" />
                </Media.Right>
            </Media>
        </Card.Content>
    </Card>
)