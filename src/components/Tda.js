import React from 'react';
import PropTypes from 'prop-types';
import Emoji from 'components/Emoji';

const Tda = ({ tda }) => (
    <span>
        <Emoji emoji={"🏁"} />&nbsp;{tda ? tda : 0}
    </span>
)

Tda.propTypes = {
    tda: PropTypes.number.isRequired,
}

export default Tda;