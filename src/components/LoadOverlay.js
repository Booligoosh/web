import React from 'react';
import styled from 'styled-components';
import Spinner from './Spinner';

let Loader = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width:      100%;
    height:     100%; 
    z-index:    0;
    top:        0;
    left:       0;
    background-color: #363636 !important;
    position:   ${props => props.coverParent ? 'absolute' : 'fixed'}; 
`

let MutedText = styled.div`
    color: ${props => props.errored ? 'red' : props.theme.primaryDarker};
`

function getErrMessage(code) {
    return code ? `Uh oh, something went wrong. (${code})` : "Uh oh, something went wrong."
}

export default ({ errored=false, code=false }) => (
    <Loader>
        <center>
            <div>
                {errored ? null : <Spinner color="white" />}
            </div>
            <MutedText errored={errored}>
                {errored ? getErrMessage() : "Go change the world."}
            </MutedText>
        </center>
    </Loader>
)