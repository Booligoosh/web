import React from 'react';
import PropTypes from 'prop-types';

// TODO: Fetch quotes from other sources.
const Quote = ({ quote, author }) => (
    <div className="quote">
        <h1 className="title is-2 no-margin begin">
            "{quote}"
        </h1>
        <h1 className="title is-4 end">{author}</h1>
    </div>
)

Quote.propTypes = {
    quote: PropTypes.string.isRequired,
    author: PropTypes.string.isRequired
}

export default Quote;