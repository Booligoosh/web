import React from 'react';
import {Tooltip} from "react-tippy";
import {Heading} from "vendor/bulma";
import {getRelatedData} from "lib/projects";
import Spinner from "./Spinner";
import {ProductList} from "features/products";
import styled from 'styled-components';

const UnderlinedText = styled.span`
    display: inline-block;
    line-height: 20px;
    color: inherit;
    border-bottom: 2px solid #2ce28a;
    margin-right: 3px;
`

class ProjectRelated extends React.Component {
    state = {
        productsReady: false,
        products: null,
        failed: false,
    }

    componentDidMount() {
        this.fetchProducts()
    }

    fetchProducts = async () => {
        try {
            const relations = await getRelatedData(this.props.project.id);
            this.setState({
                productsReady: true,
                products: relations.products,
                failed: false,
            })
        } catch (e) {
            this.setState({
                failed: true,
                productsReady: false,
                products: null,
            })
        }
    }

    render() {
        const project = this.props.project;

        if (!project) {
            return <Heading>No project provided.</Heading>
        }

        return (
            <div style={{ width: 300, padding: 10, fontSize: 16, textAlign: 'left' }}>
                <Heading>Linked products</Heading>
                {!this.state.productsReady &&
                    <Spinner small color={'white'} />
                }
                {this.state.productsReady && this.state.products && this.state.products.length > 0 &&
                    <ProductList media products={this.state.products} />
                }
                {this.state.products && this.state.products.length === 0 &&
                    <span>No products linked.</span>
                }
            </div>
        )
    }
}

const ProjectLink = (props) => {
    const tag = (
        <UnderlinedText>
            {props.children}
        </UnderlinedText>
    )

    if (!props.project) {
        return tag
    }

    return (
        <Tooltip
            interactive
            useContext
            html={
                <ProjectRelated project={props.project} />
            }
            delay={300}
            position={'top'}
            size={'small'}>
            {tag}
        </Tooltip>
    )
}

export default ProjectLink;