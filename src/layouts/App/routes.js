import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { userIsAuthenticated, userIsNotAuthenticated } from './authRules';
import LoginPage from 'pages/LoginPage';
import NotFound from "../../pages/Error/NotFound/index";
import RegisterPage from "../../pages/RegisterPage/index";
import Loadable from 'react-loadable';
import HomePage from "../../pages/HomePage/index";
import StreamPage from "../../pages/StreamPage/index";
import TasksPage from "../../pages/TasksPage/index";
import {Button, Container, Hero, Title} from "vendor/bulma";
import AboutPage from "../../pages/AboutPage/index";
import EntryPage from "../../pages/EntryPage/index";
import ForgotPage from "../../pages/ForgotPage/index";
import UserPage from "../../pages/ProfilePage/index";
import ProductPage from "../../pages/ProductPage/index";
import DiscussionsPage from "../../pages/DiscussionsPage";
import MigratePage from 'pages/MigratePage';
import MyProductsPage from "pages/MyProductsPage";
import StatsPage from 'pages/StatsPage';
import AppsPage from 'pages/AppsPage';
import Page from "../Page";
import StreamersPage from "../../pages/StreamersPage";

function Loading(props) {
    if (props.error) {
        return (
            <Hero>
                <Hero.Body>
                    <Container>
                        <Title>
                            Uh oh, something went wrong.
                        </Title>
                        <Button onClick={props.retry}>Retry</Button>
                    </Container>
                </Hero.Body>
            </Hero>
		);
    } else if (props.timedOut || props.pastDelay) {
		return <Page loading={true} />;
    } else {
        return null;
    }
}

const LoadableExplorePage = Loadable({
    loader: () => import('pages/ExplorePage'),
    loading: Loading,
})

const LoadableSettingsPage = Loadable({
    loader: () => import('pages/SettingsPage'),
    loading: Loading,
})

const LoadableOpenPage = Loadable({
    loader: () => import('pages/OpenPage'),
    loading: Loading,
})

const routes = (
    <Switch>
        <Route exact path="/" component={userIsNotAuthenticated(HomePage)}/>
        <Route path="/begin" component={userIsNotAuthenticated(RegisterPage)}/>
        <Route path="/forgot" component={userIsNotAuthenticated(ForgotPage)}/>
        <Route path="/login" component={userIsNotAuthenticated(LoginPage)}/>
        <Route path="/log" component={userIsAuthenticated(StreamPage)} />
        <Route path="/products/" exact component={userIsAuthenticated(MyProductsPage)} />
        <Route path="/products/:slug" component={ProductPage} />
        <Route path="/tasks/:id(\d+)" component={EntryPage} />
        <Route path="/tasks" component={userIsAuthenticated(TasksPage)} />
        <Route path="/stats" component={userIsAuthenticated(StatsPage)} />
        <Route path="/apps" component={AppsPage} />
        <Route path="/settings" component={userIsAuthenticated(LoadableSettingsPage)} />
        <Route path="/migrate" component={userIsAuthenticated(MigratePage)} />
        <Route path="/explore" component={LoadableExplorePage}/>
        <Route path="/live" component={StreamersPage}/>
        <Route path="/about" component={AboutPage}/>
        <Route path="/open" component={LoadableOpenPage}/>
        <Route path="/discussions" component={DiscussionsPage}/>
        <Route path='/chat' component={() => window.location = 'https://makerskitchen.xyz'}/>
        <Route path="/@:username" component={UserPage} />
        <Route component={NotFound} />
    </Switch>
)

// <Route path="/products/:id" component={ProductPage} />

export default routes;