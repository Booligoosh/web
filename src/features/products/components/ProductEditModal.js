import {deleteProduct, editProduct, getProductBySlug, leaveProduct} from "../../../lib/products";
import {isFunction} from "lodash-es";
import React from "react";
import {Button, Control, Field, Image, Message, SubTitle, Title} from "vendor/bulma";
import LaunchedToggle from "./LaunchedToggle";
import Modal from "../../../components/Modal/Modal";
import ProjectPicker from "../../projects/components/ProjectPicker";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import {File} from 'vendor/bulma';
import Spinner from "../../../components/Spinner";
import withCurrentUser from "../../users/containers/withCurrentUser";
import {Level} from "../../../vendor/bulma";
import {StreamCard} from "../../stream/components/Stream/components/StreamCard/styled";
import TeamSelector from "./TeamSelector";

class ProductEditForm extends React.Component {
    state = {
        isLoading: true,
        isSubmitting: false,
        product: null,
        finished: false,
        name: '',
        description: '',
        launched: false,
        logo: null,
        logoPreviewUrl: null,
        selectedProjects: [],
        url: '',
        productHunt: '',
        twitter: '',
        errorMessages: null,
        team: [],
    }

    fetchProduct = async () => {
        try {
            const product = await getProductBySlug(this.props.productSlug);
            this.setState({
                isLoading: false,
                product: product,
                name: product.name,
                description: product.description,
                launched: product.launched,
                logo: '',
                logoPreviewUrl: product.icon,
                url: product.website,
                productHunt: product.product_hunt,
                twitter: product.twitter,
                selectedProjects: product.projects.map(project => project.id),
                user: product.user,
            })
        } catch (e) {
            // ??
        }
    }

    onAddTeamMember = (team) => {
        this.setState({
            team
        })
    }

    componentDidMount() {
        this.fetchProduct()
    }

    onLogoUpload = (event) => {
        const file = event.target.files[0];
        const reader  = new FileReader();

        reader.addEventListener("load", (e) => {
            this.setState({
                logoPreviewUrl: reader.result
            })
        }, false);

        if (file) {
            reader.readAsDataURL(file);
        }

        this.setState({
            logo: file
        })
    }

    onSubmit = async () => {
        try {
            this.setState({ isSubmitting: true });
            const product = await editProduct(
                this.props.productSlug,
                this.state.name,
                this.state.description,
                this.state.selectedProjects,
                this.state.productHunt,
                this.state.twitter,
                this.state.url,
                this.state.launched,
                this.state.logo,
                this.state.team.map(u => u.id)
            )

            if (isFunction(this.props.onFinish)) {
                this.props.onFinish(product)
            }
        } catch (e) {
            this.setState({ isSubmitting: false, errorMessages: e.field_errors || e.message });
        }
    }

    onDelete = async () => {
        const isOwner = this.props.me.id === this.state.user;
        try {
            if (isOwner) {
                await deleteProduct(this.props.productSlug);
            } else {
                await leaveProduct(this.props.productSlug)
            }

            if (isFunction(this.props.onDelete)) {
                this.props.onDelete()
            }
        } catch (e) {
            this.setState({ isSubmitting: false, errorMessages: e.field_errors || e.message });
        }
    }

    setUrl = (key, url) => {
        let newUrl = url;

        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            newUrl = `https://${url}`
        }
        this.setState({
            [key]: newUrl
        })
    }

    renderErrorMessages = () => {
        let messages = [];
        let errors = this.state.errorMessages;
        if (typeof errors === 'object') {
            for (let key in errors) {
                messages.push(
                    <p>
                        <strong>{key.replace(/[_-]/g, " ")}</strong>: {errors[key]}
                    </p>
                )
            }
        } else if (errors.constructor === Array) {
            errors.map((err) => {
                messages.push(
                    <p>{err}</p>
                )

                return true;
            })
        } else {
            messages = this.state.errorMessages;
        }

        return messages
    }

    renderForm = () => {
        if (this.props.me.id !== this.state.user) {
            return (
                <div className="CreateProductForm">
                    <ProjectPicker
                        onProjectSelect={(projects) => this.setState({selectedProjects: projects})}
                        initialSelectedProjects={this.state.selectedProjects} />
                    {this.state.errorMessages &&
                        <Message danger>
                            <Message.Body>
                                {this.renderErrorMessages()}
                            </Message.Body>
                        </Message>
                    }
                </div>
            )
        }

        return (
            // todo: refactor this classname!
            <div className="CreateProductForm">
                <Field>
                    <SubTitle is="6">Product name</SubTitle>
                    <Control>
                        <input value={this.state.name} onChange={(e) => this.setState({ name: e.target.value })} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Product description</SubTitle>
                    <Control>
                        <textarea value={this.state.description} onChange={(e) => this.setState({ description: e.target.value })} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Website</SubTitle>
                    <Control>
                        <input value={this.state.url} onChange={(e) => this.setUrl('url', e.target.value)} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Product Hunt URL</SubTitle>
                    <Control>
                        <input value={this.state.productHunt} onChange={(e) => this.setUrl('productHunt', e.target.value)} />
                    </Control>
                </Field>
                <Field>
                    <SubTitle is="6">Twitter handle</SubTitle>
                    <Control>
                        <input value={this.state.twitter} onChange={(e) => this.setState({ twitter: e.target.value })} />
                    </Control>
                </Field>

                <Field>
                    <SubTitle is="5">Launched?</SubTitle>
                    <Control>
                        <LaunchedToggle launched={this.state.launched} onLaunchedChange={(e) => this.setState({ launched: !this.state.launched })} />
                    </Control>
                </Field>
                <hr />
                <TeamSelector
                    product={this.state.product}
                    team={this.state.team}
                    onChange={this.onAddTeamMember} />
                <hr />
                <ProjectPicker
                    onProjectSelect={(projects) => this.setState({selectedProjects: projects})}
                    initialSelectedProjects={this.state.selectedProjects} />
                <hr />
                <div className="columns">
                    <div className="column">
                        <SubTitle is="5">Icon</SubTitle>
                        <br />
                        <File name boxed>
                            <File.Label>
                                <File.Input accept="image/*" onChange={this.onLogoUpload} />
                                <File.Cta>
                                    <File.Icon>
                                        <FontAwesomeIcon icon={'upload'} />
                                    </File.Icon>
                                    <File.Label as='span'>
                                        Choose a file…
                                    </File.Label>
                                </File.Cta>
                            </File.Label>
                        </File>
                    </div>
                    <div className="column">
                        <SubTitle is="5">Logo preview</SubTitle>
                        <br />
                        {this.state.logoPreviewUrl ?
                            <Image is={"128x128"} src={this.state.logoPreviewUrl} />
                            :
                            <FontAwesomeIcon icon={'ship'} size={"6x"} />
                        }
                    </div>
                </div>
                {this.state.errorMessages &&
                <Message danger>
                    <Message.Body>
                        {this.renderErrorMessages()}
                    </Message.Body>
                </Message>
                }
            </div>
        )
    }

    render() {
        if (this.state.isLoading) return <Spinner small={true} />

        const isOwner = this.props.me.id === this.state.user;

        return (
            <StreamCard>
                <StreamCard.Header>
                    <Title is={"5"}>Editing {this.state.name}</Title>
                </StreamCard.Header>
                <StreamCard.Content style={{maxHeight: "50vh", overflow: 'auto'}}>
                    {this.renderForm()}
                </StreamCard.Content>
                <StreamCard.Footer>
                    <Level mobile style={{width: "100%"}}>
                        <Level.Left>
                            <Button className="is-rounded" outlined danger onClick={this.onDelete}>
                                {isOwner ? 'Delete product' : 'Leave team'}
                            </Button>
                        </Level.Left>
                        <Level.Right>
                            <Button className="is-rounded is-expanded" loading={this.state.isSubmitting} primary onClick={this.onSubmit}>Submit changes</Button>
                        </Level.Right>
                    </Level>
                </StreamCard.Footer>
            </StreamCard>
        )
    }
}

ProductEditForm = withCurrentUser(ProductEditForm)

const ProductEditModal = (props) => (
    <Modal
        open={props.open}
        onClose={props.onClose}
        background={'transparent'}
        flexDirection={'column'}
        percentWidth={50}
        style={{
            width: "100%"
        }}
        modalStyles={{
            display: 'flex',
            minHeight: '100vh',
            alignItems: 'center',
            justifyContent: 'center',
        }}
        className="TaskDetailModal">
        <Modal.Content verticallyCentered={true}>
            <ProductEditForm productSlug={props.productSlug} onDelete={props.onDelete} onFinish={props.onFinish} />
        </Modal.Content>
    </Modal>
);

export default ProductEditModal;