import React from "react";
import {getHostname, normalizeUrl} from "../../../../../../lib/utils/products";
import {isFunction} from "lodash-es";
import ProductEditModal from "../../../ProductEditModal";
import {Link} from "react-router-dom";
import {Button, Card, Icon, Image, Media, SubTitle, Tag, Title} from "vendor/bulma";
import withCurrentUser from "../../../../../users/containers/withCurrentUser";
import Emoji from "../../../../../../components/Emoji";
import FontAwesomeIcon from "@fortawesome/react-fontawesome";
import ProductPeople from "../../../ProductPeople";

class ProductCard extends React.Component {
    state = {
        isEditModalOpen: false,
    }

    toggleEditModal = () => this.setState({ isEditModalOpen: !this.state.isEditModalOpen })

    onFinishEditing = () => {
        this.toggleEditModal();

        if (isFunction(this.props.onEdit)) {
            this.props.onEdit();
        }
    }

    isMyProduct = () => {
        // Is logged in user in the team or is they the owner?
        return (this.props.me.id === this.props.product.user) || (this.props.product.team.find(i => this.props.me.id === i))
    }

    onDelete = () => {
        this.toggleEditModal();

        if (isFunction(this.props.onDelete)) {
            this.props.onDelete();
        }
    }

    render() {
        const product = this.props.product;

        return (
            <Card>
                <Card.Content>
                    <Media>
                        {product.icon &&
                        <Media.Left>
                            <Link to={`/products/${product.slug}`}>
                                <Image is='48x48' className={"img-rounded"} src={product.icon} alt={product.name} />
                            </Link>
                        </Media.Left>
                        }
                        <Media.Content>
                            <Link to={`/products/${product.slug}`}>
                                <Title is='5'>
                                    {product.name} {product.launched && <Tag><Emoji emoji={"🚀"} /> Launched</Tag>}
                                </Title>
                                <SubTitle is='6'>{product.description}</SubTitle>
                            </Link>
                        </Media.Content>
                        {this.isMyProduct() &&
                        <Media.Right>
                            <Button className={"is-rounded"} small onClick={this.toggleEditModal}>
                                <Icon>
                                    <FontAwesomeIcon icon={'pencil-alt'} />
                                </Icon>
                                <span>Edit</span>
                            </Button>
                        </Media.Right>
                        }
                    </Media>
                </Card.Content>
                <footer className="card-footer">
                    <p className="card-footer-item">
                        {product.website &&
                        <a href={normalizeUrl(product.website)} target="_blank" rel="noopener noreferrer">
                            <Button small text>
                                <Icon>
                                    <FontAwesomeIcon icon={'globe'} />
                                </Icon>
                                <span>
                                        {getHostname(product.website)}
                                    </span>
                            </Button>
                        </a>
                        }
                        {!product.website &&
                        <small>
                            <strong>{product.projects.length}</strong> hashtags
                        </small>
                        }
                    </p>
                    <p className="card-footer-item">
                        <ProductPeople slug={product.slug} size={32} />
                    </p>
                </footer>
                {this.isMyProduct() &&
                    <ProductEditModal
                        open={this.state.isEditModalOpen}
                        onClose={this.toggleEditModal}
                        productSlug={product.slug}
                        onDelete={this.onDelete}
                        onFinish={this.onFinishEditing} />
                }
            </Card>
        )
    }
}

export default withCurrentUser(ProductCard);
