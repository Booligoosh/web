import React from 'react';
import Emoji from "../../../../../../components/Emoji";

const StreamFinished = (props) => (
    <p style={{textAlign: 'center'}}>
        <b>That's about it! <Emoji emoji={"😝"} /></b>
    </p>
)

StreamFinished.propTypes = {}

export default StreamFinished;