import React from 'react';
import StreamCard from '../StreamCard/index';
import StreamDateHeader from "./StreamDateHeader";

class StreamSection extends React.Component {
    render() {
        let activityData = this.props.activityData;
        let date = this.props.date;

        return (
            <section className="StreamSection">
                <StreamDateHeader
                    date={date}
                    position={this.props.position}
                    canSwitchType={this.props.canSwitchType}
                    isFollowingFeed={this.props.isFollowingFeed}
                    onSwitch={this.props.onSwitch}
                />

                {Object.keys(activityData).map(
                    (key) => {
                        return <StreamCard key={activityData[key][0].id} userActivity={activityData[key]} />
                    }
                )}
            </section>
        )
    }
}

StreamSection.propTypes = {}

export default StreamSection;