import React from 'react';
import PropTypes from 'prop-types';
import Modal from "components/Modal";
import {CommentsBox} from "features/comments";
import EntryDetail from "./TaskDetail";
import withCurrentUser from "../../../../../users/containers/withCurrentUser";

class TaskDetailModal extends React.Component {
    render() {
        return (
            <Modal
                open={this.props.open}
                onClose={this.props.onClose}
                background={'transparent'}
                flexDirection={'column'}
                percentWidth={70}
                style={{
                    width: "100%"
                }}
                modalStyles={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
                className="TaskDetailModal">
               <div className={"columns"}>
                    <div className={"column"}>
                        <EntryDetail task={this.props.task} />
                    </div>
                   <div className={"column"}>
                       <CommentsBox task={this.props.task} />
                   </div>
               </div>
            </Modal>
        )
    }
}

TaskDetailModal.propTypes = {
    open: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    task: PropTypes.object.isRequired,
}

export default withCurrentUser(TaskDetailModal);