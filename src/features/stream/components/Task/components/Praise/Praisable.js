import React from 'react';
import {debounce} from "lodash-es";
import PraiseCount from "./PraiseCount";
import {withCurrentUser} from "features/users";
import {canPraise, setPraise} from "lib/tasks";
import Repeatable from "react-repeatable";
import {Button} from "../../../../../../vendor/bulma";
import Emoji from "../../../../../../components/Emoji";
import {incrementPraise} from "../../../../../../lib/tasks";
import styled from 'styled-components';

const PraiseButton = styled(Button)`
  transition: all 300ms ease;
` 

class Praisable extends React.Component {
    maxPraise = 100

    state = {
        checked: false,
        isPraising: false,
        canPraise: false,
        amount: 0,
        failed: false,
        loggedOutError: false,
        tooMuchPraise: false,
        clicked: false,
        done: false,

    }

    componentDidMount() {
    }

    onStart = async () => {
        await this.canPraise()
        this.setState({
            checked: true,
            clicked: true,
            done: false,
            amount: 0,
        })
    }

    onRelease = () => {
        this.setState({
            clicked: false,
        })
    }

    beginPraise = async () => {
        if (!this.state.checked) return;
        if (this.props.isLoggedIn &&
            this.state.canPraise &&
            this.state.amount + 1 <= this.maxPraise) {
            this.setState({
                isPraising: true,
                amount: this.state.amount + 1
            })
        } else if (!this.props.isLoggedIn) {
            this.setState({ loggedOutError: true, })
            setTimeout(() => this.setState({ loggedOutError: false, }), 2000)
        } else if(!this.state.canPraise && this.props.user.id !== this.props.task.user.id) {
            this.setState({
                tooMuchPraise: this.state.canPraise === false
            })

            setTimeout(() => this.setState({ tooMuchPraise: false, }), 2000)
        }
    }

    endPraise = async () => {
        await this.setState({
            isPraising: false,
            done: true,
        })

        setTimeout(e => this.setState({ done: false, }), 500)
        await this.setPraise()
    }

    canPraise = async () => {
        try {
            const praisable = await canPraise(this.props.task.id)
            this.setState({
                canPraise: praisable,
            })
        } catch (e) {
            this.setState({
                failed: true
            })
        }
    }

    setPraise = debounce(async () => {
        let toPraise = this.state.amount;
        try {
            await setPraise(this.props.task.id, toPraise)
        } catch (e) {
            this.setState({ failed: true })
        }
    }, 300)

    stopPropagation = () => {
        if (this.click) {
            this.click.stopPropagation()
        }
    }

    warn = () => {
        this.setState({
            clicked: true,
        })
        setTimeout(() => this.setState({ clicked: false}), 500)
    }

    incrementPraise = debounce(async () => {
        try {
            await incrementPraise(this.props.task.id, 1)
        } catch (e) {
            this.setState({ failed: true })
        }
    }, 300)

    componentDidUpdate(prevProps) {
        if (
            this.props.task &&
            prevProps.task &&
            this.props.task.praise !== prevProps.task.praise &&
            !this.state.isPraising
        ) {
            this.setState({
                amount: 0,
            })
        }
    }

    renderPraiseButton = () => (
        <Repeatable
            repeatDelay={100}
            onPress={this.onStart}
            onRelease={this.onRelease}
            onHold={this.beginPraise}
            onHoldEnd={this.endPraise}
            repeatInterval={50}
            componentClass={PraiseButton}
            className={this.state.tooMuchPraise || !this.props.isLoggedIn || this.props.me.id === this.props.task.user.id ? "is-small is-rounded disabled" : (this.state.isPraising ? "is-small is-rounded praising" : "is-small is-rounded")}>
            <Emoji emoji={"👏"} />
            {this.state.clicked && !this.state.done && "Press & hold... "}
            {this.state.done && !this.state.loggedOutError && !this.state.tooMuchPraise && "Yay! "}
            {!this.state.loggedOutError && !this.state.tooMuchPraise ? this.props.task.praise + this.state.amount : null}
            {(this.state.tooMuchPraise && this.props.isLoggedIn) && 'Too much praise!'}
            {this.state.loggedOutError && "You must login to praise."}
        </Repeatable>
    )

    render() {
        return (
            <div
                className={"Praisable"}>
                {this.props.children} &nbsp;{this.props.expanded && this.props.me.id !== this.props.task.user.id ? this.renderPraiseButton() : <PraiseCount amount={this.props.task.praise + this.state.amount}  />}
            </div>
        )
    }
}

export default withCurrentUser(Praisable);