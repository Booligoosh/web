import React from 'react';
import WeeklyStream from "./WeeklyStream";
import PropTypes from 'prop-types';

class ProductStream extends React.Component {
    render() {
        return <WeeklyStream indexUrl={`/products/${this.props.productSlug}/stream/`} />
    }
}

ProductStream.propTypes = {
    productSlug: PropTypes.string,
}

export default ProductStream;
