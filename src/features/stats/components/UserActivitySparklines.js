import React from 'react';
import PropTypes from 'prop-types';
import ActivitySparklines from "./ActivitySparklines";
import {SubTitle} from "vendor/bulma";
import Spinner from "components/Spinner";
import {connect} from 'react-redux';

const UserActivitySparklines = (props) => {
    if (props.isLoading) {
        return <Spinner small={true} />
    } else if (!props.activityTrend) {
        return <SubTitle grey>No data.</SubTitle>
    }

    return <ActivitySparklines trend={props.activityTrend} />
}

const mapStateToProps = (state) => {
    return {
        isLoading: state.stats.isLoading,
        activityTrend: state.stats.user.activity_trend,
    }
}

UserActivitySparklines.propTypes = {
    isLoading: PropTypes.bool,
    activityTrend: PropTypes.shape({
        done: PropTypes.array
    }),
}

export default connect(
    mapStateToProps,
)(ActivitySparklines)