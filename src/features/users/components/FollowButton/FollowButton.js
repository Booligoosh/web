import React from 'react';
import PropTypes from 'prop-types';
import { Icon, Button } from 'vendor/bulma';
import FontAwesomeIcon from '@fortawesome/react-fontawesome';
import { follow, unfollow, isFollowing } from 'lib/user';

class FollowButton extends React.Component {
    state = {
        isLoading: true,
        following: false,
        failed: false,
    }

    onFollowClick = async () => {
        try {
            this.setState({ isLoading: true, failed: false });
            let following = false;
            if (this.state.following) {
                following = await unfollow(this.props.userId);
            } else {
                following = await follow(this.props.userId);
            }
            this.setState({ isLoading: false, following: following, failed: false });
        } catch (e) {
            this.setState({ failed: true, isLoading: false });
        }
    }

    checkFollowing = async () => {
        try {
            this.setState({ isLoading: true, failed: false });
            const following = await isFollowing(this.props.userId);
            this.setState({ isLoading: false, following: following, failed: false });
        } catch (e) {
            this.setState({ failed: true, isLoading: false });
        }
    }

    componentDidMount() {
        this.checkFollowing();
    }

    render() {
        return (
            <Button
                className={'has-brand-green'}
                loading={this.state.isLoading}
                error={this.state.failed}
                primary
                inverted={this.props.inverted}
                outlined
                onClick={this.state.failed ? this.checkFollowing : this.onFollowClick}>
                <Icon small>
                    <FontAwesomeIcon icon={this.state.following ? 'check' : 'user-plus'} />
                </Icon>
                {this.state.following ? 'Following' : 'Follow'} {this.state.failed ? '(Failed to load. Click to try again.)' : null}
            </Button>
        )
    }
}

FollowButton.propTypes = {
    inverted: PropTypes.bool,
    userId: PropTypes.number.isRequired,
}

export default FollowButton;