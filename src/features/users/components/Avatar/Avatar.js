import React from 'react';
import PropTypes from 'prop-types';

class Avatar extends React.Component {
    getAura = () => {
        if (!this.props.withAura || !this.props.user) return null;
        const user = this.props.user;
        let aura = null;

        if (this.props.aura) {
            aura = this.props.aura
        }

        if (user.streak >= 20) {
            aura = 'long-streak'
        }

        if (user.gold) {
            aura = 'gold'
        }

        if (!aura) return null
        return `Aura ${aura}`;
    }

    getClassNames = () => {
        let classNames = "Avatar image img-circle is-square";

        if (this.props.is) {
            classNames += ` is-${this.props.is}x${this.props.is}`;
        }

        if (this.props.withBadge) {
            classNames += ' badge is-badge-white is-avatar-badge is-badge-small';
        }

        if (this.props.className) {
            classNames += ` ${this.props.className}`
        }

        return classNames
    }

    getStyles = () => {
        if (!this.getAura()) return null;
        let styles = {}

        if (this.props.is && this.props.withAura) {
            let auraPercentage = (5.5 / 100) * this.props.is;
            styles.padding = auraPercentage;
        }

        return styles
    }

    render() {
        return (
            <span
                style={{paddingTop: 0}}
                data-badge={this.props.user && this.props.user.streak ? `🔥 ${this.props.user.streak}` : null}
                className={`${this.getClassNames()} ${this.getAura()}`}>
                <img
                    style={this.getStyles()}
                    alt={this.props.user.username}
                    src={this.props.user.avatar || this.props.src} />
            </span>
        )
    }
}

Avatar.defaultProps = {
    is: 48,
    className: '',
    withAura: true
}

Avatar.propTypes = {
    is: PropTypes.number,
    className: PropTypes.string,
    withAura: PropTypes.bool,
}

export default Avatar;