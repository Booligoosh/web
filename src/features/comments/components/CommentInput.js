import React from 'react';
import {Media, SubTitle} from "vendor/bulma";
import {Link} from "react-router-dom";
import {postComment} from "lib/tasks";
import {Avatar, withCurrentUser} from "features/users";
import Spinner from "components/Spinner";
import AutoTextarea from 'react-textarea-autosize';
import styled from 'styled-components';
import FontAwesomeIcon from "@fortawesome/react-fontawesome/src/components/FontAwesomeIcon";
import {Box, Button, Level} from "../../../vendor/bulma";
import {Selector} from "react-giphy-selector";

const Textarea = styled(AutoTextarea)`
    width: 100%;
    border: none;
    box-shadow: none;
    font-family: sans-serif;
    font-size: 18px;
    outline: none;
    background-color: transparent;
    padding-top: 2px;
`

const HelpText = styled.span`
  font-size: 12px;
  color: gray;
`

const GifPicker = styled(Box)`
`

class CommentInput extends React.Component {
    state = {
        loading: false,
        content: '',
        failed: false,
        gifOpen: false,
    }

    toggleGif = () => {
        this.setState({
            gifOpen: !this.state.gifOpen,
        })
    }

    onGifSelect = async (gif) => {
        if (gif.type === 'gif' && gif.images && gif.images.original && gif.images.original.gif_url) {
            await this.setState({
                content: this.state.content + ` ![gif](${gif.images.original.gif_url})`,
                gifOpen: false,
            })
            await this.onSubmit({ preventDefault: () => {} })
        }
    }

    onSubmit = async (e) => {
        e.preventDefault()
        this.setState({loading: true})
        try {
            let comment = await postComment(this.props.taskId, this.state.content);
            this.setState({ loading: false, content: '', failed: false })
            if (this.props.onCreate) {
                this.props.onCreate(comment)
            }
        } catch (e) {
            this.setState({ loading: false, failed: true })
        }
    }

    keydownHandler = (e) => {
        // if (e.keyCode===13 && (e.ctrlKey || e.metaKey)) this.onSubmit(e)
        if (e.keyCode===13 && !e.shiftKey) this.onSubmit(e)
    }

    render() {
        if (!this.props.isLoggedIn) {
            return <SubTitle><Link to={'/begin'}>Sign in or join</Link> to post a comment.</SubTitle>
        }

        return (
            <div style={{width: "100%"}}>
                <Level style={{width: "100%"}} mobile>
                    <form onSubmit={this.onSubmit} style={{width: "100%"}}>
                        <Media className={"CommentInput"} style={{width: "100%"}}>
                            <Media.Left>
                                <Link to={`/@${this.props.user.username}`}>
                                    <Avatar is={24} user={this.props.user} />
                                </Link>
                            </Media.Left>
                            <Media.Content>
                                <Textarea style={{resize: 'none'}} onKeyDown={this.keydownHandler} placeholder={"Write a comment..."} value={this.state.content} onChange={(e) => this.setState({ content: e.target.value })}/>
                                {this.state.content.length > 0 &&
                                <HelpText><FontAwesomeIcon icon={['fab', 'markdown']} /> Shift+Enter to add a new line. Enter to finish. </HelpText>
                                }
                            </Media.Content>
                            {this.state.loading || this.props.isLoading ?
                                <Media.Right>
                                    <Spinner small />
                                </Media.Right> : null
                            }
                        </Media>
                    </form>
                    <Level.Right>
                        <Level.Item>
                            <Button
                                onClick={this.toggleGif}
                                small
                                text
                                style={{fontWeight: 'bold', letterSpacing: 1, color: 'gray', textTransform: "uppercase", textDecoration: 'none'}}>
                                GIF
                            </Button>
                        </Level.Item>
                    </Level.Right>
                </Level>

                {this.state.gifOpen &&
                    <GifPicker>
                        <Selector
                            apiKey={'x1uFeisfpQoHzLea5vVZ0myZ9R43RmIY'}
                            onGifSelected={this.onGifSelect}
                            queryInputPlaceholder={"Search a topic..."}
                            queryFormInputClassName={'input'}
                            queryFormSubmitClassName={'button is-primary'}
                            queryFormInputStyle={{borderTopRightRadius: 0, borderBottomRightRadius: 0}}
                            queryFormSubmitStyle={{borderTopLeftRadius: 0, borderBottomLeftRadius: 0}}
                            />
                    </GifPicker>
                }
            </div>
        )
    }
}

CommentInput = withCurrentUser(CommentInput);

export default CommentInput;