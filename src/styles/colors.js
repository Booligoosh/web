import chroma from 'chroma-js';

export function darker(hex, amount=0.8) {
   return chroma(hex).darken(amount)
}

export function lighter(hex, amount=0.8) {
    return chroma(hex).brighten(amount)
}

export class Colors {
    constructor(primary='#47E0A0', isDark=false) {
        this.primary = primary;
        this.primaryDarker = isDark ? darker(primary, 1.6).hex() : darker(primary).hex();
        this.isDark = isDark;
    }
}

// Expose a simple API - direct access to properties
export default Colors