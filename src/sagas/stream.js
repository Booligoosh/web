import { call, put, takeLatest, select, take, race, all } from 'redux-saga/effects';
import {eventChannel} from 'redux-saga';
import { types as streamTypes, actions as streamActions } from '../ducks/stream';
import { types as editorTypes } from 'ducks/editor';
import { actions as tasksActions } from 'ducks/tasks';
import axios from 'axios';
import { getStreamMetadata } from 'lib/tasks';
import RWS from 'reconnecting-websocket';

export const getStreamState = (state) => state.stream
export const getUserState = (state) => state.user
const getToken = (state) => state.auth.token

function* initStream(action) {
    // fetch initial metadata n stuff
    yield put(streamActions.loadMore());
    yield put(streamActions.connect());
}

function* loadMore(action) {
    let streamState = yield select(getStreamState);

    try {
        let nextUrl = streamState.nextUrl;

        if ((!streamState.initialLoaded && !nextUrl) || action.type === editorTypes.TASK_CREATE_SUCCEED) {
            // get initial link
            const streamMetadata = yield call(
                getStreamMetadata,
                streamState.isFollowingFeed
            );

            nextUrl = streamMetadata.latest_url;
        }

        // we now have metadata. go ahead, let's ROLL!
        if (nextUrl) {
            // get the stream data
            const response = yield call(
                axios.get,
                nextUrl
            );

            yield put(streamActions.streamSuccess(response.data.data, response.data.previous_url));
        } else {
            yield put(streamActions.streamAllLoaded())
        }
    } catch (e) {
        yield put(streamActions.streamFailed(e.message))
    }
}

function getStreamSocketUrl(following, token=null) {
    let path = '/explore/stream/';
    if (following) {
        path = `/stream/?token=${token}`;
    }
    return `${process.env.REACT_APP_WS_URL}${path}`
}

function getLastUpdatedTime(streamState) {
    let lastUpdatedTime = null;

    if (streamState.lastUpdatedTime) {
        lastUpdatedTime = streamState.lastUpdatedTime;
    } else {
        // yes, keep the [0]
        lastUpdatedTime = streamState.data[0] ? streamState.data.slice(-1)[0].updated_at : null;
    }

    if (!lastUpdatedTime && !streamState.initialLoaded) {
        return false
    }

    return lastUpdatedTime
}

function* sendListener(socket) {
    while (true) {
        yield take('STREAM_SEND_SOCKET');
        // socket.send(JSON.stringify({ type: 'setTask', status: 'open' }))
    }
}

function* syncListener(socket) {
    while (true) {
        yield take('STREAM_SOCKET_SYNC');
        let streamState = yield select(getStreamState);
        let lastTime = getLastUpdatedTime(streamState)
        if (lastTime) {
            let ev = JSON.stringify({
                type: 'task.sync',
                payload: {
                    last_date: lastTime
                }
            })
            console.log("Makerlog: Requesting stream sync over sockets.", ev)
            socket.send(ev)
        }
        // socket.send(JSON.stringify({ type: 'setTask', status: 'open' }))
    }
}

function* receiveListener(socketChannel) {
    // if anything is emitted from `listen`, this dispatches it.
    while (true) {
        const action = yield take(socketChannel);
        yield put(action);
    }
}

function listen(socket, user) {
    return eventChannel((emit) => {
        socket.onopen = () => {
            console.log("Makerlog: Stream connection established.")
        };
        socket.onmessage = (event) => {
            const data = JSON.parse(event.data);
            console.log(`Makerlog: Event received through WS. (${data.type})`, data.payload)
            switch(data.type) {
                case 'task.created':
                case 'task.updated':
                case 'task.sync':
                    if (data.batch) {
                        console.log("Makerlog: Batch received, merging...")
                        emit(streamActions.merge(data.payload))
                        data.payload.map(task => {
                            if (task.user.id === user.id) {
                                console.log("Makerlog: Batch received contains own task.")
                                emit(tasksActions.loadTasksSuccess([task], task.created_at))
                            }

                            return true
                        })
                    } else {
                        console.log("Makerlog: Merging task to stream...")
                        emit(streamActions.merge([data.payload]))
                        if (data.payload.user.id === user.id) {
                            console.log("Makerlog: Task received is user's task. Merging to tasks...")
                            emit(tasksActions.loadTasksSuccess([data.payload], data.payload.created_at))
                        }
                    }
                    break;

                case 'task.deleted':
                    emit(streamActions.removeTask(data.payload.id));
                    break;

                default:
                    return;
            }
        };
    return () => {
     console.log("Makerlog: Stream connection closed.")
      socket.close();
    };
  });
}

function* streamSocketWatcher(action) {
    let user = yield select(getUserState);
    // wait for persist to load
    while (Object.keys(user.me).length === 0) {
        user = yield select(getUserState);
        yield take();
    }
    console.log("Makerlog: Store loaded.")

    while (true) {
        let streamState = yield select(getStreamState);
        let token = yield select(getToken);
        let user = yield select(getUserState)
        yield take("STREAM_SOCKET_OPEN");
        const socket = new RWS(getStreamSocketUrl(streamState.isFollowingFeed, token));
        const socketChannel = yield call(listen, socket, user.me);
        const { cancel } = yield race({
            task: all(
                [
                    call(receiveListener, socketChannel),  // listen to receive msg
                    call(sendListener, socket),// listen to send new msg
                    call(syncListener, socket) // listen to sync
                ]
            ),
            cancel: take('STREAM_SOCKET_CLOSE')
        });

        if (cancel) {
            socketChannel.close();
        }
    }
}

function* streamInitSaga() {
    yield takeLatest(streamTypes.STREAM_INIT, initStream)
}

function* streamLoadSaga() {
    // replace with pollImmediately
    yield takeLatest(streamTypes.STREAM_FETCH_REQUEST, loadMore);
}

export {
    streamInitSaga,
    streamLoadSaga,
    streamSocketWatcher
}