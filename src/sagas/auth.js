import { call, put, takeLatest, race, take } from 'redux-saga/effects';
import { types as authTypes, actions as authActions } from '../ducks/auth';
import { actions as userActions, types as userTypes } from 'ducks/user';
import axios from 'axios';
import { getToken } from 'lib/auth';


function* fetchToken(action) {
	try {
		let token = null;

		if (action.token) {
			token = action.token
		} else {
            token = yield call(
                getToken,
                action.username,
                action.password
            );
		}

		axios.defaults.headers.common['Authorization'] = `Token ${token}`;

        yield put(userActions.loadUser());

        const { error } = yield race({
            success: take(userTypes.USER_SUCCESS),
            error: take(userTypes.USER_FAILED),
        })

        if (error) {
            console.log("!!!FETCH TOKEN RACE CONDITION!!!");
            console.log("This was screwed with for the obj error warning.");
            throw new Error("Race condition fetching user.");
        }

		yield put(authActions.loginSuccess(token));
	} catch (e) {
		let action = null;
		action = authActions.loginFailed(e.message)
		yield put(action)
	}
}


function* loginSaga() {
	yield takeLatest(authTypes.LOGIN_REQUESTED, fetchToken)
}

export {
	loginSaga
}