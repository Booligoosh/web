export function asset(path) {
    return process.env.PUBLIC_URL + path
}