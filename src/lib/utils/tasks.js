import React from 'react';
import {groupBy, orderBy} from "lodash-es";
import processString from "react-process-string";
import ProjectLink from "../../components/ProjectLink";
import Fuse from "fuse.js";
import {uniqBy} from "lodash-es";
import {Link} from "react-router-dom";

const fuseOptions = {
    shouldSort: true,
    tokenize: true,
    matchAllTokens: true,
    threshold: 0.6,
    location: 0,
    distance: 100,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    keys: [
        "content"
    ]
};

function groupTasksByProject(tasks, orderByDate=false) {
    // Get all projects available.
    // accumulate all project sets
    // delete dupe projects
    let projects = {};

    let sortedTasks = tasks;

    if (orderByDate) {
        sortedTasks = orderBy(
            [...tasks],
            (task) => new Date(task.done_at),
            ['desc']
        );
    }

    sortedTasks.map((task) => {
        if (task.project_set.length !== 0) {
            task.project_set.map((project) => {
                !(project.name in projects) && (projects[project.name] = []);
                projects[project.name].push(task);
                return null;
            })
        } else {
            !("Miscellaneous" in projects) && (projects["Miscellaneous"] = []);
            projects["Miscellaneous"].push(task);
            return null;
        }

        return null;
    });

    return projects;
}

function getProjectsFromTasks(tasks) {
    if (!tasks) return null;

    let projects = []
    tasks.map(task => {
        projects.push(...task.project_set);
        return true;
    })

    return uniqBy(projects, 'id')
}

function groupTasksByDone(tasks) {
    let resultObj = {'done': [], 'in_progress': [], 'remaining': []}
    let newObj = groupBy(tasks, (task) => {
        if (task.done) {
            return "done";
        } else if (task.in_progress) {
            return "in_progress"
        } else {
            return "remaining";
        }
    })

    return {...resultObj, ...newObj}
}

function sortByDone(array) {
    return array.sort(item => item.done === false);
}

function orderByDate(data, order='desc')  {
    return orderBy(data, ['created_at'], [order])
}

function orderByUpdated(data, order='desc')  {
    return orderBy(data, ['updated_at'], [order])
}

function dateWithoutTime(date) {
    date.setHours(0, 0, 0, 0);
    return date;
}

function sortStreamByActivity(data) {
    if (data) {
        let orderedData = orderBy(
            data,
            ['created_at', 'done_at'],
            ['desc', 'desc']
        );
        // TODO: Order sortStreamByActivity by Date
        let groupedByDate = groupBy(orderedData, (obj) => {
            // this is the big deal here!
            let date = new Date(obj.done_at || obj.created_at);
            return dateWithoutTime(date);
        });
        let groupedByUser = {}

        Object.keys(groupedByDate).forEach((key) => {
            groupedByUser[key] = groupBy(groupedByDate[key], (obj) => obj.user.username);
        })

        return groupedByUser
    }
}

function processTaskString(task) {
    const mentionsConfig = {
        regex: /(^| )@[a-z0-9_-]+/gi,
        fn: (key, result) => {
            const username = result[0];

            return (
                <Link to={`/${username.trim()}`} target="_blank" rel="noopener noreferrer" key={key}>{username}</Link>
            )
        }
    }

    const hashtagConfig = {
        regex: /#(\w+)/g,
        fn: (key, result) => {
            if (!result[1]) return result[0];
            const projectName = result[1];
            let foundProjects = task.project_set.filter(project => project.name.toUpperCase() === projectName.toUpperCase());

            if (!foundProjects.length)
                return result[0]; //#username

            return (
                <ProjectLink project={foundProjects[0]} key={key}>#{projectName}</ProjectLink>
            )
        }
    }

    return processString([mentionsConfig, hashtagConfig])(task.content);
}

function processMentions(string) {
    const mentionsConfig = {
        regex: /\B@[a-z0-9_-]+/gi,
        fn: (key, result) => {
            const username = result[0];

            return (
                <Link to={`/${username}`} target="_blank" rel="noopener noreferrer" key={key}>{username}</Link>
            )
        }
    }

    return processString([mentionsConfig])(string);
}

function applySearchTerms(tasks, searchTerms) {
    if (tasks) {
        let searchedTasks = tasks;
        if (searchTerms) {
            const fuse = new Fuse(tasks, fuseOptions);
            searchedTasks = fuse.search(searchTerms);
        }

        return searchedTasks;
    }

    return tasks;
}

function wasAddedToday(task) {
    return new Date(task.created_at).toDateString() === new Date().toDateString()
}

export {
    groupTasksByDone,
    groupTasksByProject,
    sortByDone,
    orderByDate,
    orderByUpdated,
    sortStreamByActivity,
    dateWithoutTime,
    applySearchTerms,
    wasAddedToday,
    getProjectsFromTasks,
    processTaskString,
    processMentions
}